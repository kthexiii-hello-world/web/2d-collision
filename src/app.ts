import DebugLogger from './scripts/DebugLogger'

import ProtorEngine from './scripts/ProtorEngine/ProtorEngine'
import Shape2D from './scripts/ProtorEngine/Shape2D'
import Color from './scripts/ProtorEngine/Color'

const canvas: HTMLCanvasElement = document.createElement('canvas')
document.getElementById('renderer-container').appendChild(canvas)

const Engine = new ProtorEngine(canvas, '2d')

window.addEventListener('resize', onWindowResize, false)

const DEBUG = new DebugLogger(
  {
    loopStatusLog: true,
    overlay: true,
    graph: true,
  },
  stopStart
)

const autoStart: boolean = true

const gravity: Array<number> = [0, -15]

let ballRadius: number = 10
let ballVeolocity: Array<number> = [20, 10]
let ballPosition: Array<number> = [Engine.getWidth() / 2, ballRadius]

main()

function main() {
  DEBUG.loopStatus(autoStart)
  if (autoStart) animate()
}

function update() {
  ballVeolocity[0] += gravity[0] * Engine.deltaTime
  ballVeolocity[1] -= gravity[1] * Engine.deltaTime

  ballPosition[0] += ballVeolocity[0]
  ballPosition[1] += ballVeolocity[1]

  if (ballPosition[1] + ballRadius * 1.1 >= Engine.getHeight()) {
    ballVeolocity[1] = -ballVeolocity[1]
    ballPosition[1] = Engine.getHeight() - ballRadius * 1.1
  }

  if (ballPosition[1] - ballRadius * 1.1 <= 0) {
    ballVeolocity[1] = -ballVeolocity[1]
    ballPosition[1] = ballRadius * 1.1
  }

  if (ballPosition[0] + ballRadius * 1.1 >= Engine.getWidth()) {
    ballVeolocity[0] = -ballVeolocity[0]
    ballPosition[0] = Engine.getWidth() - ballRadius * 1.1
  }

  if (ballPosition[0] - ballRadius * 1.1 <= 0) {
    ballVeolocity[0] = -ballVeolocity[0]
    ballPosition[0] = ballRadius * 1.1
  }
}

Engine.render = () => {
  Shape2D.background('#121212')

  Engine.fillStyle('#ffffff')

  Shape2D.fillCirlce(ballPosition, ballRadius)
}

function animate() {
  update()

  Engine.update()
  Engine.render()

  DEBUG.update()

  Engine.animationID = requestAnimationFrame(animate)
}

function stopStart() {
  if (Engine.animationID) {
    Engine.pause()
  } else {
    animate()
  }

  DEBUG.loopStatus(Engine.animationID != null)
}

function onWindowResize(event: UIEvent) {
  Engine.resize()
}

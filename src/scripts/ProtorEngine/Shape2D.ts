import Renderer from './Renderer'

class Shape2D {
  static background(style: string) {
    Renderer.context.fillStyle = style
    Renderer.context.fillRect(
      0,
      0,
      Renderer.canvas.width,
      Renderer.canvas.height
    )
  }

  static lineTo(a: Array<number>, b: Array<number>) {
    Renderer.context.beginPath()
    Renderer.context.moveTo(a[0], a[1])
    Renderer.context.lineTo(b[0], b[1])
    Renderer.context.closePath()
    Renderer.context.stroke()
  }

  static cirlce(origin: Array<number>, radius: number) {
    Renderer.context.beginPath()
    Renderer.context.arc(origin[0], origin[1], radius, 0, Math.PI * 2)
    Renderer.context.closePath()
  }

  static fillCirlce(origin: Array<number>, radius: number) {
    Renderer.context.beginPath()
    Renderer.context.arc(origin[0], origin[1], radius, 0, Math.PI * 2)
    Renderer.context.closePath()
    Renderer.context.fill()
  }

  static strokeCirlce(origin: Array<number>, radius: number) {
    Renderer.context.beginPath()
    Renderer.context.arc(origin[0], origin[1], radius, 0, Math.PI * 2)
    Renderer.context.closePath()
    Renderer.context.stroke()
  }
}

export default Shape2D

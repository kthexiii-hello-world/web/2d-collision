import Renderer from './Renderer'

class ProtorEngine extends Renderer {
  animationID: number

  presentTime: number = 0
  deltaTime: number = 0
  pastTime: number = 0 // in ms

  offset: Array<number> = [0, 0]
  scale: Array<number> = [0, 0]

  /**
   * ProtorEngine, abstracting the html canvas.
   * @param canvas Canvas element
   * @param mode Canvas context mode
   */
  constructor(canvas: HTMLCanvasElement, mode: string) {
    super(canvas, mode)
    this.resize()
  }

  update() {
    this.presentTime = performance.now()
    this.deltaTime = (this.presentTime - this.pastTime) / 1000
    this.pastTime = this.presentTime
  }

  pause() {
    this.deltaTime = 0
    cancelAnimationFrame(this.animationID)
    this.animationID = null
  }

  resize() {
    const displayWidth = Math.floor(
      Renderer.canvas.clientWidth * devicePixelRatio
    )
    const displayHeight = Math.floor(
      Renderer.canvas.clientHeight * devicePixelRatio
    )

    // Check if the canvas is not the same size.
    if (
      Renderer.canvas.width != displayWidth ||
      Renderer.canvas.height != displayHeight
    ) {
      // Make the canvas the same size
      Renderer.canvas.width = displayWidth
      Renderer.canvas.height = displayHeight
    }
  }

  /**
   * Convert World space to Screen space
   * @param worldSpace Input World space. Number array with x,y as component
   * @param screenSpace  Output Screen Space. Number array with x,y as component
   */
  worldToScreenSpace(worldSpace: Array<number>, screenSpace: Array<number>) {
    screenSpace[0] = (worldSpace[0] - this.offset[0]) * this.scale[0]
    screenSpace[1] = (worldSpace[1] - this.offset[1]) * this.scale[1]
  }

  /**
   * Convert Screen space to World space
   * @param screenSpace Input Screen space. Number array with x,y as component
   * @param worldSpace Output World Space. Number array with x,y as component
   */
  screenToWorldSpace(screenSpace: Array<number>, worldSpace: Array<number>) {
    worldSpace[0] = screenSpace[0] / this.scale[0] + this.offset[0]
    worldSpace[1] = screenSpace[1] / this.scale[1] + this.offset[1]
  }

  fillStyle(style: string) {
    Renderer.context.fillStyle = style
  }

  strokeStyle(style: string) {
    Renderer.context.strokeStyle = style
  }

  stroke() {
    Renderer.context.stroke()
  }

  fill() {
    Renderer.context.fill()
  }
}

export default ProtorEngine

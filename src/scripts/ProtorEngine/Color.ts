class Color {
  static Hex(hex: number) {
    return `#${hex.toString(16)}`
  }

  static Red() {
    return '#ff0000'
  }

  static Green() {
    return '#00ff00'
  }

  static Blue() {
    return '#0000ff'
  }

  static Black() {
    return '#000000'
  }

  static White() {
    return '#ffffff'
  }
}

export default Color

class Renderer {
  static canvas: HTMLCanvasElement
  mode: string

  static context: CanvasRenderingContext2D
  static gl: WebGLRenderingContext
  static gl2: WebGL2RenderingContext

  resolutionScale: number = 1

  constructor(canvas: HTMLCanvasElement, mode: string) {
    Renderer.canvas = canvas

    switch (mode) {
      case '2d':
        Renderer.context = canvas.getContext('2d')
        break
      case 'webgl':
        Renderer.gl = canvas.getContext('webgl')
        break
      case 'webgl2':
        Renderer.gl = canvas.getContext('webgl2')
        break
      default:
        break
    }

    Renderer.canvas.style.height = '100%'
    Renderer.canvas.style.width = '100%'
    Renderer.canvas.id = 'ProtorEngine-renderer'
  }

  render() {}

  setResolutionScale(scale: number) {
    this.resolutionScale = scale
  }

  getWidth() {
    return Renderer.canvas.width
  }

  getHeight() {
    return Renderer.canvas.height
  }
}

export default Renderer
